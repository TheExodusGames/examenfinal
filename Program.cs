using System;


namespace _8
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculosYFunciones _Funciones = new CalculosYFunciones();   
            Console.WriteLine();
            Console.WriteLine("Bienvenido a la Calculadora de Prestamos Coronado.");
            Console.WriteLine("A continuacion, digite los datos necesarios.");
            Console.WriteLine("------------------------------------------");
            Console.Write("Introduce el monto del prestamo: ");
            _Funciones.Mdp = double.Parse(Console.ReadLine());
            Console.Write("Introduce la tasa de interes anual: ");
            _Funciones.Tia = double.Parse(Console.ReadLine());
            Console.Write("Introduce el plazo en meses: ");
            _Funciones.Plazos = int.Parse(Console.ReadLine());
            Console.WriteLine("--------------------------------------------------------------------------------------");
            Console.WriteLine("Monto del prestamo en RD$:\t\t\t\t {0}", _Funciones.Mdp);
            Console.WriteLine("Tasa de Porcentaje Anual: \t\t\t\t {0}", _Funciones.Tia + " %");
            Console.WriteLine("Plazo: \t\t\t\t\t\t\t {0}", _Funciones.Plazos + " Meses");
            _Funciones.inicio();
            _Funciones.MostrarEnPantalla();

        }
    }

    class CalculosYFunciones
    {
        /*
        TIM = Tasa interes mensual
        TIA = Tasa interes anual.
        Mdp = Monto del prestamo.
        Ip= Interes Pagado.
        Cp = Capital Pagado.
        Pagos = Cuotas.
        I = Contador.
        Plazos = Meses en los que se dividira el monto.

        */
        public double Pagos, IP, Cp, Mdp, Tia, Tim;
        public int  Orden, Plazos, i;
        public void MostrarEnPantalla()
        {
            Tim = (Tia / 100) / 12;


            Pagos = Tim + 1;
            Pagos = (double)Math.Pow(Pagos, Plazos);
            Pagos = Pagos - 1;
            Pagos = Tim / Pagos;
            Pagos = Tim + Pagos;
            Pagos = Mdp * Pagos;


            Orden = 1;
            Console.WriteLine("--------------------------------------------------------------------------------------");
            Console.WriteLine();
            Console.WriteLine("\t\t\t\t\t----------------------------------");
            Console.WriteLine("\t\t\t\t\t\tTabla de Amortizacion");
            Console.WriteLine("\t\t\t\t\t----------------------------------");
            Console.WriteLine();
            Console.Write("Numero de pago\t");
            Console.Write("Fecha de pago \t\t");
            Console.Write("Cuota \t\t\t");
            Console.Write("Capital \t\t");
            Console.Write("Interes \t");
            Console.Write("Balance \t");
            Console.WriteLine();
            Console.WriteLine();



            for (i = 1; i <= Plazos; i++)
            {

             
                Console.Write("{0}\t\t", Orden);

                var dat = new DateTime(2021, 4, 30);
                Console.Write(dat.AddMonths(i).ToString("d"));
                Console.Write("\t\t{0:N2}\t", Pagos);

               
                IP = Tim * Mdp;
                Cp = Pagos - IP;
                Console.Write("\t{0:N2}\t",Cp);
                Console.Write("\t{0:N2}\t", IP);


                Mdp = Mdp - Cp;
                Console.Write("\t{0:N2}\t", Mdp);

                Orden = Orden + 1;
                Console.WriteLine();

            }

            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------------------------");
            Console.WriteLine("Gracias por usar nuestro sistema de Prestamos. BY: ALEXANDER RAFAEL NUNEZ LOPEZ.");
            Console.WriteLine("--------------------------------------------------------------------------------");
        }
        public void inicio()
        {
            Tim = (Tia / 100) / 12;

            Pagos = Tim + 1;
            Pagos = (double)Math.Pow(Pagos, Plazos);
            Pagos = Pagos - 1;
            Pagos = Tim / Pagos;
            Pagos = Tim + Pagos;
            Pagos = Mdp * Pagos;

            Console.WriteLine("Valor Cuota: \t\t\t\t\t\t RD$ {0:N2}", Pagos);
        }
        }
    }
